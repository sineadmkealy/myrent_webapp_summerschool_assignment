//By default the property name used in the validation object will match against the 
// - id, 
// - name, or 
// - data-validate property of each input to find the corresponding field to match validation 
// rules against.

$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();

$('.ui.form').form({
	fields : {
		'firstName' : {
			identifier : 'firstName',// matches name="landlord.firstName"
			rules : [ {
				type : 'empty',
				prompt : 'Please select a landlord first name'
			} ]
		},
		'lastName' : {
			identifier : 'lastName',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please select a landlord last name'
			} ]
		},
		'email' : {
			identifier : 'email',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter your email address'
			} ]
		},
		'password' : {
			identifier : 'password',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter a password'
			}, {
				type : 'length[6]',
				// appears on prompt if all correct info not submitted:
				prompt : 'Your password must be at least 6 characters'
			} ]
		},
		//---------------------------------------------------------------
		'address1' : {
			identifier : 'address1',// matches name="landlord.firstName"
			rules : [ {
				type : 'empty',
				prompt : 'Please select the address first line'
			} ]
		},
		'address2' : {
			identifier : 'address2',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please select the address second line'
			} ]
		},
		'city' : {
			identifier : 'city',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter the city'
			} ]
		},
		'county' : {
			identifier : 'county',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter the county'
			} ]
		},		
		'postcode' : {
			identifier : 'postcode',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter the postcode'		
			} ]
		}
	}
});
