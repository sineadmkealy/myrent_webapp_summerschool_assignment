//$('.ui.dropdown').dropdown() // Javascript code to activate filters/dropdown menu

// Story 17 semantic validation
// uses jquery
$('#byRented').dropdown();// uses dropdown id selector
$('#byType').dropdown(); 
$('#byRent').dropdown();

//SEMANTIC VALIDATION:
//uses form name attribute to distinguish between the two dropdowns having same parameter 'eircode'
//Semantic 2:the form parameter is a single object containing fields: {...} name-value pair
	 $('.ui.form[name="byRentedForm"]').form({ 
		 fields: {
			 'filter' : { 
				 identifier : 'filter',// matches name=".."
				 rules : [{ 
				 type : 'empty', 
					 prompt : 'Please click on the icon to select your filter' 
			        },],
		      },
		    },
	  });
	 
$('.ui.form[name="byTypeForm"]').form({ 
	 fields: { 
		 'filter' : { 
			 identifier : 'filter',// matches name=".."
			 rules : [{ 
			 type : 'empty', 
				 prompt : 'Please click on the icon to select your filter' 
			 },],
	      },
	    },
 });

$('.ui.form[name="byRentForm"]').form({ 
fields: { 
	 'filter' : { 
		 identifier : 'filter',// matches name=".."
		 rules : [{ 
		 type : 'empty', 
			 prompt : 'Please click on the icon to select your filter' 
	        },],
	      },
	    },
});
	


//AJAX:-??????
/*$( document ).ready(function() {
	$('#bytype').dropdown();
	$('#byrented').dropdown();
	$('#bysort').dropdown();
});*/