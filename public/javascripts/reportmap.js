/*! Javascript code to locate the highlight circle on the map */

// let map; // map variable is located outside the function initialize, ie. a global variable.
let circle;

function initialize() {
  const center = new google.maps.LatLng(53.347298, -6.268344);
  const initRadius = 10000;
  const mapProp = {
    center: center,
    zoom: 7,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  };
  // informs the Google API where the map is to be rendered (to obtain the DOM element)
  const mapDiv = document.getElementById('map_canvas');
  const map = new google.maps.Map(mapDiv, mapProp);
  // map = new google.maps.Map(mapDiv, mapProp);
  mapDiv.style.width = '500px';
  mapDiv.style.height = '500px';

//A circle object created by invoking the google.maps.Circle constructor.
//Arbitrarily, the centre of the map matches the centre of the circle.
  circle = new google.maps.Circle({
    center: center,
    radius: initRadius,
    strokeColor: '#0000FF',
    strokeOpacity: 0.4,
    strokeWeight: 1,
    fillColor: '#0000FF',
    fillOpacity: 0.4,
    draggable: true,
  });
  circle.setEditable(true);//allows radius resizing by dragging anchor point
  circle.setMap(map);// renders the circle on the map
}

// method interrogates the circle object to obtain the circle centrepoint and radius
// transmits these using jQuery to the div placeholders in the html file
function requestReport() {
	  const center = circle.getCenter();
	  const latcenter = center.lat().toString();
	  const lngcenter = center.lng().toString();
	  const radius = circle.getRadius().toString();
	  $('#radius').val(radius);
	  $('#latcenter').val(latcenter);  
	  $('#lngcenter').val(lngcenter);
	}
google.maps.event.addDomListener(window, 'load', initialize);


