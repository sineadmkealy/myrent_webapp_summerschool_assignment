//By default the property name used in the validation object will match against the 
// - id, 
// - name, or 
// - data-validate property of each input to find the corresponding field to match validation 
// rules against.

$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();

$('.ui.form').form({
	fields : {
		'landlord.firstName' : {
			identifier : 'landlord.firstName',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please select a landlord first name'
			} ]
		},
		'landlord.lastName' : {
			identifier : 'landlord.lastName',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please select a landlord last name'
			} ]
		},
		'landlord.email' : {
			identifier : 'landlord.email',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter your email address'
			} ]
		},
		'messageTxt' : {
			identifier : 'messageTxt',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter your message'
			} ]
		}
	}
});
