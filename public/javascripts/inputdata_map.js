function initialize() {
	//the map variable is located outside the function initialize, thus making it a global variable
	let map; 
	let marker;
	const latlng = new google.maps.LatLng(53.347298, -6.268344);	
	const mapOptions = {
		zoom : 8,
		center : new google.maps.LatLng(53.347298, -6.268344),
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	const mapDiv = document.getElementById('map_canvas');
	map = new google.maps.Map(mapDiv, mapOptions);
//	mapDiv.style.width = '500px';
//	mapDiv.style.height = '600px';
	// place a marker
	marker = new google.maps.Marker({
		map : map,
		draggable : true,
		position : latlng,
		title : "Drag and drop on your property!"
	});
	// To add the marker to the map, call setMap();
	marker.setMap(map);
	//add listeners so that updated data is automatically transmitted to the views 
	//(not necessary to press buttons) when (circle overlay is moved, resized) or 
	//if marker is moved
	// marker listener populates hidden fields ondragend
	// ie updates maker overlay coordinates when drag ends
	google.maps.event.addListener(marker, 'dragend', function() {
		//interrogates the google.maps.Marker object to obtain the marker's position:
		const latLng = marker.getPosition();
		//converts the position (a LatLng object) to a single string:
		const latlong = latLng.lat().toString().substring(0, 10) + ','
				+ latLng.lng().toString().substring(0, 10);
		// publish lat long in geolocation control in inputdata.index html page
		//uses jQuery to transmit the string to the view:
		$("#geolocation").val(latlong);
		// update the new marker position
		map.setCenter(latLng);
	});
}
google.maps.event.addDomListener(window, 'load', initialize);