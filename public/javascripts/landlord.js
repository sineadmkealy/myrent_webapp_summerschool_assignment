/*$( document ).ready(function() {	//ensures the DOM is ready before the code executes
    $('#editresidence').dropdown();
    $('#deleteresidence').dropdown();
});	*/
// --------------------------------------------
// CANNOT GET WORKING USING NAME SELECTOR
//$( document ).ready(function() { 
$('#editresidence').dropdown(); // uses dropdown id selector
 $('#deleteresidence').dropdown(); // uses dropdown id selector

// SEMANTIC VALIDATION:
 //uses form name attribute to distinguish between the two dropdowns having same parameter 'eircode'
//Semantic 2:the form parameter is a single object containing fields: {...} name-value pair
	 $('.ui.form[name="deleteresidenceform"]').form({ 
		 fields: {
			 'eircode' : { 
				 identifier : 'eircode',// matches name=".."
				 rules : [{ 
				 type : 'empty', 
					 prompt : 'Select residence to delete' 
			        },],
		      },
		    },
	  });
	 
$('.ui.form[name="editresidenceform"]').form({ 
	 fields: { 
		 'eircode' : {
		 identifier : 'eircode',// matches name=".." 
		 rules : [{ 
			 type : 'empty',
		 prompt : 'Select residence to edit' 
	        },],
	      },
	    },
});
	 
//-------------------------------------------------------------------------
/*// CANNOT GET WORKIONG USING CLASS SELECTOR RATHER THAN ID
// Use a class selector instead of an id:
$('.ui.dropdown.deleteresidence').dropdown();
$('.ui.dropdown.editresidence').dropdown();

// SEMANTIC VALIDATION
*//**
 * 
 * Validate delete landlord dropdown Invoke controller action and retrieve data
 * 
 *//*
$('.ui.form.deleteresidence').form({
	fields : {
		'eircode' : {
			identifier : 'eircode',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Select residence to delete'
			}, ],
		},
	},
});

// SEMANTIC VALIDATION
*//**
 * Validate delete tenant dropdown Invoke controller action and retrieve data
 * using jQuery ajax
 *//*
$('.ui.form.editresidence').form({
	fields : {
		'eircode' : {
			identifier : 'eircode', // matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Select residence to edit'
			}, ],
		},
	},
});*/


