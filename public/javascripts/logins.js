//By default the property name used in the validation object will match against the 
// - id, 
// - name, or 
// - data-validate property of each input to find the corresponding field to match validation 
// rules against.

$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();

$('.ui.form').form({
	// semantic 1 (no fields)
	'email' : { // matches name="email"
		identifier : 'email',
		rules : [ {
			type : 'empty',
			// appears on prompt if all correct info not submitted:
			prompt : 'Please enter your email address'
		}, ]
	},
	'password' : { // matches
		identifier : 'password',
		rules : [ {
			type : 'empty',
			// appears on prompt if all correct info not submitted:
			prompt : 'Please enter a password'
		}, {
			type : 'length[6]',
			// appears on prompt if all correct info not submitted:
			prompt : 'Your password must be at least 6 characters'
		} ]
	}
});