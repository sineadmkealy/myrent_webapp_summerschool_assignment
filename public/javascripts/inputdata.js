/*! initialize the controls in the input data template and validate residence type */

$('.ui.dropdown').dropdown(); // Javascript code to activate the dropdown menu
//Javascript code to initialise radio button group to indicate if residence rented or vacant: 
$('.ui.checkbox').checkbox(); 

//SEMANTIC VALIDATION: connects to InputData view :
$('.ui.form').form({
	fields : {
	'geolocation' : {
		identifier : 'geolocation',// matches name="......"
		rules : [ {
			type : 'empty',
			prompt : 'Please drag marker on map to appropriate geolocation'
		} ]
	},
	'eircode' : {
		identifier : 'eircode',// matches name=".."
		rules : [ {
			type : 'empty',
			prompt : 'Please manually enter the eircode or use eircode finder below to obtain'
		} ]
	},
	'rent' : {
		identifier : 'rent',// matches name=".."
		rules : [ {
			type : 'empty',
			prompt : 'Please enter the rent amount'
		} ]
	},
	//-----------------------------------------------------------------
	'numberBedrooms' : {
		identifier : 'numberBedrooms',// matches name=".."
		rules : [ {
			type : 'empty',
			prompt : 'Please enter the number of bedrooms'
		} ]
	},
	'numberBathrooms' : {
		identifier : 'numberBathrooms',// matches name=".."
		rules : [ {
			type : 'empty',
			prompt : 'Please enter the number of bathrooms'
		} ]
	},
	'area' : {
		identifier : 'area',// matches name=".."
		rules : [ {
			type : 'empty',
			prompt : 'Please enter the floor area in sq.m.'
		} ]
	},
	'residenceType' : {
		identifier : 'residenceType',// matches name=".."
		rules : [ {
			type : 'empty',
			prompt : 'Please select the residence type'
		} ]
	}
}
});


//----------------------------------------------------------------------------------------
//SEMANTIC VALIDATION:
/*$('.ui.form')
.form({
  residenceType : {
    identifier : 'residence.residenceType',
    rules: [
      {
          type : 'empty',
          prompt: 'Please select a residence type'
      }
    ]
  }
});*/

//AJAX CALL:
/*{
    onSuccess : function() {
        submitForm();
        return false;
    } 
});

//use this to post a notification message on successful return from the Ajax call
// If the validation succeeds the function submitForm is invoked:
function submitForm() {
    var formData = $('.ui.form.segment input').serialize(); 
    $.ajax({
      type: 'POST',
      url: '/inputdata/datacapture',
      data: formData,
      success: function(response) { 
          console.log("notification: " + response.inputdata);
          $('#notification').html(response.inputdata);
      }
    });
  }
});*/