//By default the property name used in the validation object will match against the 
// - id, 
// - name, or 
// - data-validate property of each input to find the corresponding field to match validation 
// rules against.

$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();

$('.ui.form').form({ // uses jQuery
	//Semantic 2:the form parameter is a single object containing fields: {...} name-value pair
	fields : {
		'tenant.firstName' : {
			identifier : 'tenant.firstName',// matches name="landlord.firstName"
			rules : [ {
				type : 'empty',
				prompt : 'Please select a landlord first name'
			} ]
		},
		'tenant.lastName' : {
			identifier : 'tenant.lastName',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please select a landlord last name'
			} ]
		},
		'tenant.email' : {
			identifier : 'tenant.email',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter your email address'
			} ]
		},
		'tenant.password' : {
			identifier : 'tenant.password',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter a password'
			}, {
				type : 'length[6]',
				// appears on prompt if all correct info not submitted:
				prompt : 'Your password must be at least 6 characters'
			} ]
		},
		'conditions' : {
			identifier : 'conditions',
			rules : [ {
				type : 'checked',
				// appears on prompt if all correct info not submitted:
				prompt : 'You must agree to the terms and conditions'
			} ]
		}
	}
});
