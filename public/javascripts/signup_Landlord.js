//By default the property name used in the validation object will match against the 
// - id, 
// - name, or 
// - data-validate property of each input to find the corresponding field to match validation 
// rules against.

$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();

$('.ui.form').form({
	//Semantic 2:the form parameter is a single object containing fields: {...} name-value pair
	fields : { 
		'landlord.firstName' : {
			identifier : 'landlord.firstName',// matches name="landlord.firstName"
			rules : [ {
				type : 'empty',
				prompt : 'Please select a landlord first name'
			} ]
		},
		'landlord.lastName' : {
			identifier : 'landlord.lastName',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please select a landlord last name'
			} ]
		},
		'landlord.email' : {
			identifier : 'landlord.email',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter your email address'
			} ]
		},
		'landlord.password' : {
			identifier : 'landlord.password',// matches name=".."
			rules : [ {
				type : 'empty',
				prompt : 'Please enter a password'
			}, {
				type : 'length[6]',
				// appears on prompt if all correct info not submitted:
				prompt : 'Your password must be at least 6 characters'
			} ]
		},
		'conditions' : {
			identifier : 'conditions',
			rules : [ {
				type : 'checked',
				prompt : 'You must agree to the terms and conditions'// appears on prompt if all correct info not submitted
			} ]
		}
	}
});
