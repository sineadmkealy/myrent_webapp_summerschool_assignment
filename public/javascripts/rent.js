$('.ui.dropdown').dropdown(); // Javascript code to activate the dropdown menu

// SEMANTIC VALIDATION working:
$('.ui.form').form({
    'eircode' : {
        identifier : 'eircode',
        rules : [{
          type : 'empty',
          prompt : 'Select a vacant residence (if available) before pressing button'
		} ]
	}
});

// Where an Ajax call is required, the form parameter is a single object comprising 2 name:value pairs:
//fields: { ...}
//onSuccess: {...}

/*$( document ).ready(function() {  
   $('#eircodeResidence').dropdown(); // uses dropdown id selector
    
   // SEMANTIC VALIDATION:
// Use a class selector instead of an id:
    $('.ui.form.changeTenancy').form({
      fields : {
        eircode : {
          identifier : 'eircode',
          rules : [{
            type : 'empty',
            prompt : 'Select a vacant residence (if available) before pressing button'
          },],
        },
      },
  
      // AJAX:
      onSuccess : function(event, fields) {
        changeTenancy();      
        event.preventDefault();
       },           
    });*/

   /* function changeTenancy() {
      var formData = $('.ui.form.changeTenancy').serialize(); 
      let $eircodeNewRental = $('#eircode_residence').dropdown('get text');
      let $eircodeOldRental = $("#exist_eircode").val(); // Exist rent control
      $("#exist_eircode").val($eircodeNewRental); // change Existing rental control
      $.ajax({ // THE jquery function $() or jQuery() . A jQuery function (ajax)
        type : 'POST', // HTTP method
        url : '/tenants/changetenancy',  // Routes for controller action name
        data : formData, // the form data
        success : function(response) { // Asynchronous callback function
        // process the response data in this block:
          // response format: eircode, lat, lng, marker message
          //CIRCLEMAP.updateMarkers(response);   
          CIRCLEMAP.refreshMarkers();
          updateTenantDropdown($eircodeOldRental, $eircodeNewRental);
        }
      });
    }

    function updateTenantDropdown($eircodeOldRental, $eircodeNewRental) {
      let $obj = $('.item.eircode');
      // Remove the selected eircode from dropdown
      for (let i = 0; i < $obj.length; i += 1) {
        if($obj[i].getAttribute('data-value').localeCompare($eircodeNewRental) == 0) {
          $obj[i].remove();
          $('#eircode_residence').dropdown('clear');
          break;
        }
      }
      
      // Add the new rental eircode to dropdown
      let newMenuItem = dropdownDiv($eircodeOldRental);
      $('.menu.tenant').append(newMenuItem);
    }
    
    function dropdownDiv(name) {
      return '<div class="item eircode"' + ' ' + 'data-value="' + name + '">' + name + '</div>';
    }*/
//}); 