/*! Javascript code to activate the map :
 * render the map
 * facilitate dragging of an overlay (a marker) and
 * capturing of the latitude and longitude coordinates of the last position of the marker.*/

function initialize() {
    var map;
    var marker;
    var latlng = new google.maps.LatLng(53.347298, -6.268344);

    var mapOptions = {
        zoom : 8,
        center : new google.maps.LatLng(53.347298,-6.268344),
        mapTypeId : google.maps.MapTypeId.ROADMAP
    };
    //refers to div id="map_canvas" in InputData.index.html:
    //This determines the map location within the view
    var mapDiv = document.getElementById('map_canvas');
    map = new google.maps.Map(mapDiv,mapOptions);
    mapDiv.style.width = '500px';
    mapDiv.style.height = '600px';
    // place a marker
        marker = new google.maps.Marker({
        map : map,
        draggable : true,
        position : latlng,
        title : "Drag and drop on your property!"
    });
    // To add the marker to the map, call setMap();
    marker.setMap(map); 
    //marker listener populates hidden fields ondragend
    google.maps.event.addListener(marker, 'dragend', function() {
        var latLng = marker.getPosition();
        var latlong = latLng.lat().toString().substring(0,10) + ',' + latLng.lng().toString().substring(0,10);
        //publish lat long in geolocation control in html page
        //geolocation is the id of the div in the html file where the data will be written (InputData.index) (Location)
        //The data to be written is the parameter of the method val, namely latlong
        //This statement captures the latitude and longitude of the marker at the conclusion of dragging and renders 
        //it using JQuery to the html element whose id is "geolocation
        $("#geolocation").val(latlong); //method call on a jQuery object
        //update the new marker position
        //latlng is an object of type LatLng and contains the geolocation of the marker
        map.setCenter(latLng);
      });
}
google.maps.event.addDomListener(window, 'load', initialize);