//USED?

package utils;

import java.util.Comparator;

import models.Residence;

public class ResidenceRentComparator implements Comparator<Residence> {

	public String sortDirection;

	/**
	 * Method to introduce the sortDirection parameter, required for dropdown
	 * 
	 * @param sortDirection
	 *            ascending or descending sort direction
	 */
	public ResidenceRentComparator(String sortDirection) {
		this.sortDirection = sortDirection;
	}

	/**
	 * Compares the rent attributes of each residence uses the Residence
	 * compareTo method using appropriate attribute of Residence b as the
	 * parameter
	 */
	@Override
	public int compare(Residence a, Residence b) {
		if (sortDirection.equals("Sort_ascending")) {
			return Long.compare(a.rent, b.rent); // compare A against B
		} else {
			return Long.compare(b.rent, a.rent); // compare B against A
		}
	}
}

/*
 * DRAFTS: Attempt using Collections.sort and Collections.reverse - .reverse()
 * doesn't work with comparator public int compare(Residence a, Residence b) {
 * return Long.compare(a.rent, b.rent); // compare A against B } }
 * 
 * @Override public int compare(Residence a, Residence b) {
 * 
 * int rent1 = (int) a.rent; // cast long rent to int type int rent2 = (int)
 * b.rent;
 * 
 * if (rent1 == rent2) return 0; else if (rent1 > rent2) return 1; else //(rent2
 * > rent1) return -1; }
 * 
 * @Override public int compare(Long o1, Long o2) { return o2.compareTo(o1); }
 * 
 * @Override public int compare(Residence a, Residence b) { return
 * a.rent.compareTo(b.rent); } }
 */
