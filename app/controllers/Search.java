package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Landlord;
import models.Residence;
import models.Tenant;
import play.Logger;
import play.mvc.Before;
import play.mvc.Controller;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

public class Search extends Controller {
	/**
	   * This method executed before each action call in the controller.
	   * Checks that a landlord has logged in.
	   * If no landlord logged in the landlord is presented with the log in screen.
	   */
	  @Before
	  static void checkAuthentification() {
	    if(session.contains("logged_in_tenantid") == false)
	      Tenants.login();
	  }

	  /**
	   * Generates a Report instance relating to all VACANT residences within circle
	   * @param radius    The radius (metres) of the search area
	   * @param latcenter The latitude of the centre of the search area
	   * @param lngcenter The longtitude of the centre of the search area
	   */
	  public static void generateSearchResults(double radius, double latcenter, double lngcenter) {
		    // All reported residences will fall within this circle
		    Circle circle = new Circle(latcenter, lngcenter, radius);
		    Tenant tenant = Tenants.getCurrentTenant();
		    List<Residence> residences = new ArrayList<Residence>();
		    // Fetch all vacant residences and filter out those within circle:
		    List<Residence> residencesAll = Residence.findAll();
		    for (Residence res : residencesAll){
		      LatLng residenceLocation = res.getGeolocation();
		      
		      // For VACANT residences within the circle area
		      if (Geodistance.inCircle(residenceLocation, circle) && (res.tenant == null)) {
		    	  residences.add(res);
		        Logger.info("residence added");
		        // falling down here......
		      }
		    }
		    //to render the actual generateSearchResults report page directly, incorporating model and utils data
		    render("Search/report.html", tenant, circle, residences);
		  }

	  /**
	   * Render the views/ReportController/index.html template
	   * This presents a map and resizable circle to indicate a search area for residences
	   */
	  public static void index() {
	    render();
	  }
	}


