
package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

/**
 * An Landlords class to inherit data and behaviour from the Controller class
 * Note: static methods may be invoked from any controller, minimising code
 * duplication.
 * 
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */
public class Landlords extends Controller {

	/**
	 * Creates the landlord signup for rendering in the Landlords signup view
	 */
	public static void signup() {
		render();
	}

	/**
	 * Creates the landlord signup for rendering in the Landlords login view
	 * Registers a landlord onto the MyRent system
	 * 
	 * @param landlord
	 *            Simulates the logged-in landlord
	 */
	public static void register(Landlord landlord) {
		landlord.save();
		Logger.info("register :" + landlord.firstName);
		login();
	}

	/**
	 * Creates the landlord login for rendering in the Landlords login view
	 */
	public static void login() {
		Logger.info("Attempting to login as landlord");
		render();
	}

	/**
	 * Creates the landlord logout by diverting the landlord to the Welcome
	 * landing page
	 */
	public static void logout() {
		//session.clear();
		setSessionLogout();
		Logger.info("Landlord signed out");
		Welcome.index();
	}
	

	/**
	 * Affirms the current landlord by searching the firstName of landlord for
	 * the current session.  Authenticates that a landlord's login credentials
	 * are valid. The Play session tracks key information such as the logged-in user id.
	 * 
	 * @param email
	 *            The submitted email address
	 * @param password
	 *            The corresponding password
	 * @return the currently logged-in landlord
	 **/
	public static void authenticate(String email, String password) {
		Logger.info("Attempting to authenticate with " + email + ":" + password);

		Landlord landlord = Landlord.findByEmail(email);
		if ((landlord != null) && (landlord.checkPassword(password) == true)){
		// refers to landlord model
			Logger.info("Landlord Authentication successful");
			session.put("logged_in_landlordid", landlord.id);
			// to ensure tenant cannot access value for landlordid and go to the Landlord page:
			session.put("logged_in_tenantid", null);
			session.put("logged_status", "logged_in");
			Landlords.index();
		} else {
			Logger.info("Authentication failed");
			login();
		}
	}

	/**
	 * Affirms the currently logged-in landlord
	 * 
	 * @return
	 */
	public static Landlord getCurrentLandlord() {
		String landlordId = session.get("logged_in_landlordid");
		if (landlordId == null) {
			return null;
		}
		Landlord logged_in_landlord = Landlord.findById(Long.parseLong(landlordId));
		// refers to landlord model
		Logger.info("Logged in landlord is " + logged_in_landlord.firstName);
		return logged_in_landlord;
	}
	//SOLUTION
/*	  public static Landlord getCurrentLandlord() {
	    Landlord landlord = null;
	    if (session.contains("logged_in_landlordid"))   {
	      String landlordId = session.get("logged_in_landlordid");
	      landlord = Landlord.findById(Long.parseLong(landlordId));
	    }
	    return landlord;
	  }*/
	
	/*
	 * Clears session on logout
	 */
/*	protected static void setSessionLogout() {
		if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in")) {
			//session.clear();
			String landlordId = session.get("logged_in_landlordid");
			Landlord logged_in_landlord = Landlord.findById(Long.parseLong(landlordId));

			session.put("logged_status", "logged_out");
			//request.getSession(true); //create new session if there is no session
			request.getSession(false); //returns current session. If current session does not exist then it will create new session.
			((logged_in_landlord)session).invalidate(); //to destroy session..?
			session.invalidate();
		}
	}*/
	
		protected static void setSessionLogout() {
	if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in")) {
		String landlordId = session.get("logged_in_landlordid");
		Landlord logged_in_landlord = Landlord.findById(Long.parseLong(landlordId));
		session.remove(); // to clear cache..?????
		Logger.info("Landlord " + logged_in_landlord.firstName + " is now logged out");
		session.put("logged_status", "logged_out");
	}
}
		//ORIGINAL METHOD
/*	protected static void setSessionLogout() {
		if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in")) {
			session.clear();
			session.put("logged_status", "logged_out");
		}
	}*/

	/**
	 * Renders the profile page for the logged-in landlord
	 */
	public static void index() {
		Landlord landlord = Landlords.getCurrentLandlord();
		if (landlord == null) {
			Logger.info("Unable to getCurrentLandlord");
			Landlords.login();
		} else {
			Logger.info("Currentlandlord identified");
			render(landlord); // want to render residences here also...????
		}
	}
	

	/**
	 * Renders the Edit Profile page
	 */
	public static void editProfile() { // STORY 6
		Landlord landlord = Landlords.getCurrentLandlord();
		if (landlord == null) {
			Logger.info("Landlord class : Unable to getCurrentLandlord");
			Landlords.login();
		} else {
			Logger.info("Landlord :  Currentlandlord identified");
			render(landlord);
		}
	}

	/**
	 * Permits editing of the Landlord profile
	 * 
	 * @param firstName
	 * @param lastName
	 * @param address1
	 * @param address2
	 * @param city
	 * @param county
	 * @param postcode
	 */
	public static void changeProfile(String firstName, String lastName, String address1, String address2, String city,
			String county, String postcode) {
		String landlordId = session.get("logged_in_landlordid");
		Landlord landlord = Landlord.findById(Long.parseLong(landlordId));
		landlord.firstName = firstName;
		landlord.lastName = firstName;
		landlord.address1 = address1;
		landlord.address2 = address2;
		landlord.city = city;
		landlord.county = county;
		landlord.postcode = postcode;

		Logger.info("Landlord Profile is being changed to " + firstName + " " + lastName + " , Address: " + address1
				+ address2 + city + county);
		landlord.save();
		Landlords.index();
	}


	/**
	 * STORY 6
	 * Controller to activate the the Add residence button in Landlord Configuration page
	 */
	public static void addResidence(long rent, long numberBedrooms, int numberBathrooms,
			int area, String residenceType, String geolocation, Date dateRegistered, String eircode, Landlord landlord, Tenant tenant) {
		//Boolean rented, omitted in story 8
		InputData.index();
	}
	
	/**
	 * STORY 7
	 * Deletes a residence from the dropdown list in Landlord Configuration page
	 */
	public static void deleteResidence(String eircode) { 
		// To ascertain the current landlord user:
		Landlord landlord = Landlords.getCurrentLandlord();
		Residence residence = Residence.findByEircode(eircode);		
		Logger.info("Residence: " + residence);	
		
		landlord.residences.remove(residence); // to remove from landlord list
		Logger.info("Residence deleted: Landlord: " + landlord + " Residence: " + residence + " Eircode: " + eircode); 
	
		landlord.save(); // refers to the database: saves user updated status, with residence removed
		residence.delete(); // delete residence from the database
		index(); //refreshes the page
	}

/**
 * STORY 7
 * Renders the edit residence page for the selected residence
 */
	public static void editResidence(String eircode) {
		Landlord landlord = Landlords.getCurrentLandlord();
		Residence residence = Residence.findByEircode(eircode);
		Logger.info("Editing Residence: Landlord: " + landlord + "Residence: " + residence + "Eircode: " + eircode);
		render(residence);
	}
	
	/**
	 * STORY 7
	 * Updates of the rent field for residence object.  Eircode is displayed as read-only so 
	 * not listed as parameter.
	 */
	public static void updateResidence(String eircode, Long rent) {
		Landlord landlord = Landlords.getCurrentLandlord();
		Residence residence = Residence.findByEircode(eircode);
		Logger.info("Updating Residence: " + residence + ", eircode: " + eircode);	
		
		residence.rent = rent; // new rent value to be saved
		residence.save(); // save for the residence object
		Logger.info("Residence updated: Landlord:" + landlord +  "Residence: " + residence + " , eircode: " + eircode); 
		Landlords.index(); // divert back to landlord configuration page
	}
	
	
	}
