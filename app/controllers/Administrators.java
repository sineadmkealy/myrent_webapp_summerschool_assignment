package controllers;

import play.*;
import play.mvc.*;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

import java.util.*;

import javax.persistence.CascadeType;

import models.*;

public class Administrators extends Controller {

	/**
	 * Rendering the Administrators login view Registers an Adminstrator onto
	 * the MyRent system
	 * 
	 * @param landlord
	 *            Simulates the logged-in Administrator
	 */
	public static void register(Administrator administrator) {
		administrator.save();
		login();
	}

	/**
	 * Creates the landlord login for rendering in the Landlords login view
	 */
	public static void login() {
		Logger.info("Attempting to login as Administrator");
		render();
	}

	/**
	 * Administrator logout by diverting to the Welcome landing page
	 * 
	 */
	public static void logout() {
		setSessionLogout();
		Logger.info("Administrator signed out");
		Welcome.index();
	}

	/**
	 * Affirms the Administrator by searching for the email for the current
	 * session. Authenticates that a Adminstrator's login credentials are valid.
	 * The Play session tracks key information such as the logged-in user id.
	 * 
	 * @param email
	 *            The submitted email address
	 * @param password
	 *            The corresponding password
	 * @return the currently logged-in landlord
	 **/
	public static void authenticate(String email, String password) {
		Logger.info("Attempting to authenticate Administrator with " + email + ":" + password);

		Administrator administrator = Administrator.findByEmail(email);
		if ((administrator != null) && (administrator.checkPassword(password) == true)) {
			// refers to Administrator model
			Logger.info("Administrator Authentication successful");
			session.put("logged_in_administratorid", administrator.id);
			// WRONG??? - exactly what needed ultimately...??
			// to ensure administrator cannot access value for landlordid and
			// tenantid go to the respective pages ...???:
			// session.put("logged_in_landlordid", null);
			// session.put("logged_in_tenantid", null);
			session.put("logged_status", "logged_in");
			Administrators.index();
		} else {
			Logger.info("Authentication failed");
			Administrators.login();
		}
	}

	/**
	 * Affirms the currently logged-in landlord
	 * 
	 * @return
	 */
	public static Administrator getCurrentAdministrator() {
		String administratorId = session.get("logged_in_administratorid");
		if (administratorId == null) {
			return null;
		}
		Administrator logged_in_administrator = Administrator.findById(Long.parseLong(administratorId));
		// refers to Administrator model
		Logger.info("Logged in Administratorid is " + logged_in_administrator.email);
		return logged_in_administrator;
	}

	protected static void setSessionLogout() {
		if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in")) {
			String administratorId = session.get("logged_in_administratorid");
			Administrator logged_in_administrator = Administrator.findById(Long.parseLong(administratorId));
			session.remove(); // to clear cache..?????
			Logger.info("Administrator " + logged_in_administrator.email + " is now logged out");
			session.put("logged_status", "logged_out");
		}
	}

	/**
	 * Renders the Administration page for the logged-in administrator. Passes
	 * the Administrator, the list of landlords, residences and list of tenants
	 * to the view in order to populate dropdown menus.
	 */
	public static void index() {
		Administrator administrator = Administrators.getCurrentAdministrator();
		if (administrator == null) {
			Logger.info("Unable to getCurrentAdministrator");
			Administrators.login();
		} else {
			Logger.info("CurrentAdministrator identified");
			// pass the list of landlords to the view:
			List<Landlord> landlords = Landlord.findAll();
			List<Tenant> tenants = Tenant.findAll();
			render(administrator, landlords, tenants); //
		}
	}

	/**
	 * Find a list of marker geolocations, assembled in equivalent of json array
	 * Maps via with function retrieveMarkerLocations() in adminstrators_map.js
	 * Tooltips display via function fitBounds(latlngStr)in adminstrators_map.js
	 * geoObj[0] is eircode geoObj[1] is latitude geoObj[2] is longitude
	 * geoObj[3] is rented status message Renders all VacantResidences as a json
	 * array
	 */
	public static void geolocations() {
		// List of residences and rented status returned as part of <Residence>
		// residences >() list

		List<Residence> residencesAll = Residence.findAll();
		// administrator_map.js javascript requires an array of strings:
		List<ArrayList<String>> jsonArray = new ArrayList<ArrayList<String>>();

		for (Residence res : residencesAll) {
			ArrayList<String> s = new ArrayList<String>();

			if (res.tenant == null) {
				s.add(res.eircode);
				// latitude and longitude defined within geolocation;
				// getGeolocation a method in Residence model:
				s.add(Double.toString(res.getGeolocation().getLatitude()));
				s.add(Double.toString(res.getGeolocation().getLongitude()));
				s.add("Vacant Residence"); // to add a status messagee
				jsonArray.add(s);
				Logger.info("Vacant Residence: " + res.eircode);

			} else {
				s.add(res.eircode);
				// latitude and longitude defined within geolocation;
				// getGeolocation a method in Residence model:
				s.add(Double.toString(res.getGeolocation().getLatitude()));
				s.add(Double.toString(res.getGeolocation().getLongitude()));
				s.add("Tenant is : " + res.tenant); // to add a status messagee
				jsonArray.add(s);
				Logger.info("Residence rented: " + res.eircode);
			}
		}
		renderJSON(jsonArray);
	}

	/**
	 * Uses (CascadeType.ALL in Landlord model) mapping on residence side of
	 * relationship to delete the landlord, their residences and any associated
	 * residence tenants from the database and adjust the map markers
	 * accordingly.
	 * 
	 * @param email
	 *            The landlord object's email address
	 */
	// String eircode: cannot include as 1 parameter only for dropdown menu
	public static void deleteLandlord(String email) { 
		// " andassociated residences: "+ eircode + :need local variable
		Logger.info("Landlord: " + email + " to be deleted"); 

		Landlord landlord = Landlord.findByEmail(email);
		landlord.delete(); // delete landlord from the database
		Logger.info("Landlord deleted from database: " + landlord);
		index(); // refreshes the page incl deleting the relevant markers form
					// map?????
	}

	/*
	 * public static void deleteLandlord(String email) { // String eircode:
	 * cannot include as 1 parameter only for dropdown menu Logger.info(
	 * "Landlord: " + email + " to be deleted"); //
	 * " and associated residences: " + eircode + : need local variable //While
	 * Loop to remove the tenant from the residence so that the residence can be
	 * deleted, and then delete the landlord. Landlord landlord =
	 * Landlord.findByEmail(email); //While the landlord has residences while
	 * (landlord.residences.size() > 0){ // Get the first residence Residence
	 * residence = landlord.residences.get(0); // If the residence has a tenant,
	 * then remove the residence from the tenant if (residence.tenant != null){
	 * residence.tenant.residence = null; residence.tenant.save(); } // Remove
	 * the first residence from the landlord landlord.residences.remove(0);
	 * Logger.info("Landlord's residence removed from ist and map: " + email);
	 * // Delete the residence from the database residence.delete();
	 * Logger.info("Landlord's residence removed from Database: " + email); }
	 * landlord.delete(); // delete landlord from the database index();
	 * //refreshes the page }
	 */

	/*
	 * To delete the landlord, their residences must be removed. In residence,
	 * the reference to Landlord must also be deleted Do not need delete the
	 * residences' tenant but sets their residence value to null automatically
	 * via (OneToOne but not cascade relationship)
	 * 
	 * // WHILE LOOP: While the landlord has residences: while
	 * (landlord.residences.size() > 0) { // Clear the landlord from the first
	 * residence (at position 0 in list): landlord.residences.get(0).landlord =
	 * null; //landlords.remove(landlord); doesn't work Logger.info("Landlord "
	 * + email + " deleted from associated residence" );
	 * 
	 * // Remove the first residence at position 0:
	 * landlord.residences.remove(0); //landlord.residences.remove(residence);
	 * doesn't work Logger.info("Landlord's residence removed: " + email); //
	 * the second residence at position 1 becomes the first residence for the
	 * next iteration } landlord.delete(); // delete landlord from the database
	 * Logger.info("Landlord deleted from database: " + landlord); index();
	 * //refreshes the page incl deleting the relevant markers form map????? }
	 */

	/**
	 * Deletes the tenant from the Database, the tenant reference on the map
	 * markers and from dropdown list of tenants.
	 * 
	 * @param email
	 *            The tenant object's email address
	 */
	public static void deleteTenant(String email) {
		Logger.info("tenant: " + email + " to be deleted");
		Tenant tenant = Tenant.findByEmail(email);

		if (tenant.residence != null) { // only one tenant per residence
			// Clear the tenant from the residence
			// But Do not delete the residence
			tenant.residence = null;
			// tenants.remove(tenant); doesn't work - only for lists??
			Logger.info("Tenant: " + email + " removed from residence");
		}
		tenant.delete();// delete tenant from the databaseindex();
		Logger.info("Tenant deleted from database: " + tenant);
		index(); // refreshes the page
	}



}
