package controllers;

import play.mvc.Controller;

/**
 * This class configured only for localhost
 * @author Sinead
 *
 */
public class Acknowledgement extends Controller{
  public static void index(){
    render();
  }
}