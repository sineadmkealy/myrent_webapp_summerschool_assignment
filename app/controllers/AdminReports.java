package controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import models.Administrator;
import models.Filters;
import models.Landlord;
import models.Residence;
import models.Tenant;
import play.Logger;
import play.mvc.Controller;
import utils.ResidenceRentComparator;

public class AdminReports extends Controller {

	/**
	 * Renders the Administration Reports page for the logged-in administrator.
	 * Passes the Administrator, list of residences and associated tenants and
	 * landlords to view. Also passes the filters mdoel to view to allow
	 * selection of required filter ie. byRented, byRent, byResidenceType and
	 * filterAll (in reverse date order)
	 */
	public static void index() {
		Administrator administrator = Administrators.getCurrentAdministrator();
		List<Residence> residences = Residence.findAll();
		// List<Landlord> landlords = Landlord.findAll(); don't need to pass to
		// view, contained within residences model
		// List<Tenant> tenants = Tenant.findAll();

		// renders page differently depending on which filter selected:
		Filters filters = new Filters();

		render(administrator, residences, filters);
	}

	// ---------------------------------------------------------------------------------------
	/**
	 * Filters the list of residences by rented or vacant status. Refers to
	 * Filters model to attain the appropriate filter definitions for the
	 * byRented filter
	 * 
	 * @param filter
	 *            byRented filter: vacant or rented properties
	 */
	public static void filterByRented(String filter) {
		Logger.info("Filtering residences byRented " + filter);
		Administrator administrator = Administrators.getCurrentAdministrator();

		List<Residence> residences = new ArrayList<Residence>();

		// Fetch all residences and filter out those rented or vacant:
		List<Residence> residencesAll = Residence.findAll();
		for (Residence res : residencesAll) {

			// Pass to a list of rented residences:
			if (filter.equals("rented")) {
				if (res.tenant != null) {
					residences.add(res);
					Logger.info("Creating a list of rented residences");
				}
			}
			// Pass to a list of vacant residences:
			else if (filter.equals("vacant")) {
				// Pass to a list of vacant residences:
				if (res.tenant == null) {
					residences.add(res);
					Logger.info("Creating a list of vacant residences");
				}
			}
			// syntax convention; For all other eventualities (shouldn't
			// arise)...:
			else {
				residences.add(res);
			}
		}

		Filters filters = new Filters(); // Confirm using a new filter
		filters.filter = filter; // display byRented, the current filter
		Logger.info("Count " + residences.size()); // size of current list

		renderTemplate("AdminReports/index.html", administrator, residences, filters);
	}

	// ---------------------------------------------------------------------------------------
	/**
	 * Filters the list of residences by rented or vacant status. Refers to
	 * Filters model to attain the appropriate filter definitions for the
	 * byRented filter
	 * 
	 * @param residenceType
	 *            - Apartment|Flat, Studio or House
	 */
	public static void filterByType(String filter) {
		Logger.info("Filtering residences byResidenceType " + filter);
		Administrator administrator = Administrators.getCurrentAdministrator();
		// RESIDENCE TYPES:

		List<Residence> residences = new ArrayList<Residence>();

		// Fetch all residences and filter out the types:
		List<Residence> residencesAll = Residence.findAll();
		for (Residence res : residencesAll) {

			// Pass to a list of "Apartment|Flat" residences:
			if (filter.equals("Apartment|Flat")) {
				if (res.residenceType.equals("Apartment|Flat")) {
					residences.add(res);
					Logger.info("Creating a list of Apartment|Flat residences");
				}
			}
			// Pass to a list of "Studio" residences:
			else if (filter.equals("Studio")) {
				// Pass to a list of vacant residences:
				if (res.residenceType.equals("Studio")) {
					residences.add(res);
					Logger.info("Creating a list of Studio residences");
				}
			}
			// Pass to a list of "House" residences:
			else if (filter.equals("House")) {
				// Pass to a list of vacant residences:
				if (res.residenceType.equals("House")) {
					residences.add(res);
					Logger.info("Creating a list of House residences");
				}
			}
			// syntax convention; For all other eventualities (shouldn't
			// arise)...:
			else {
				residences.add(res);
			}
		}
		Filters filters = new Filters(); // Confirm using a new filter
		filters.filter = filter; // display byType, the current filter
		Logger.info("Count " + residences.size()); // size of current list

		renderTemplate("AdminReports/index.html", administrator, residences, filters);
	}

	// ---------------------------------------------------------------------------------------
/**
 * Filters the list of residences by rent in ascending or descending order. Refers to
	 * Filters model to attain the appropriate filter definitions for the
	 * byRented filter
 * @param filter byRent filter, sorted in ascending or descending order
 */
	public static void filterByRent(String filter){
		Logger.info("Filtering residences byRent sort direction: filter " + filter);
		Administrator administrator = Administrators.getCurrentAdministrator();

		// Fetch all residences and sort by rent amount sortOrder (ascending or descending):
		List<Residence> residences = Residence.findAll();
		Collections.sort(residences, new ResidenceRentComparator(filter));
		
	Filters filters = new Filters(); // Confirm using a new filter
	filters.filter = filter; // display byRent, the current filter
	Logger.info("Count " + residences.size()); // size of current list

	renderTemplate("AdminReports/index.html", administrator, residences, filters);		
	}

	/*
	 * // DRAFT: but Collections.reverse won't compile w comparator sort in in
	 * ascending order: if (filter.equals("ascending")){ residences.add(res);
	 * //List<Residence> residences = new
	 * ArrayList<Residence>(residence.rent);...???? Collections.sort(residences,
	 * new ResidenceRentComparator(filter)); Logger.info(
	 * "Sorting Residences in ascending order");
	 * 
	 * } else if (filter.equals("descending")) {
	 * Collections.reverse(residences); Logger.info(
	 * "Sorting Residences in descending order"); }
	 * 
	 * // syntax convention; For all other eventualities (shouldn't arise)...:
	 * else { residences.add(res); }
	 */
	// ---------------------------------------------------------------------------------------
	public static void filterAll() {
		Administrator administrator = Administrators.getCurrentAdministrator();
		List<Residence> reverseResidences = new ArrayList<Residence>();
		Collections.reverse(reverseResidences);
		Logger.info("Generating a list of residences in reverse order of registering to the db");
		renderTemplate("AdminReports/index.html", administrator, reverseResidences);
	}

}
