package controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import models.Administrator;
import models.Landlord;
import models.Residence;
import play.Logger;
import play.mvc.Before;
import play.mvc.Controller;

public class AdminCharts extends Controller {
	  
	/**
	 * Renders the current administrator, landlords and residences.rent to the view
	 */
	public static void index() {
		Administrator administrator = Administrators.getCurrentAdministrator();
		List<Landlord> landlords = Landlord.findAll();
		List<Residence> residences = Residence.findAll();

		render(administrator, landlords, residences);
	}
	
/**
 * Finds a list of the various landlord's rentroll income 'RentLandlord' as a percentage 
 * of the DB total rentroll 'sumAllRents', assembled in the equivalent of json array.
 * Maps via the function dataArray(residences)in piechart.js
 * Tooltips display via function render(residences): toolTipContent, with the label followed by the number.
 * Piechart displays via function chart_data (via method getPercentageTotal()) in piechart.js
 *  Assembles residence data from the residence model for CanvasJS pie chart display
 */
	public static void getPercentageTotal(){
		double sumAllRents = 0; // use double to return decimal values, notwithstanding rent field is 'long'
		Administrator administrator = Administrators.getCurrentAdministrator();
		
		List<Landlord> landlords = Landlord.findAll();
		List<Residence> residencesAll = Residence.findAll();
		//return a JSON object of list
		List<List<String>> Arrayjs = new ArrayList<List<String>>();
		
		for (Residence res : residencesAll) {
			sumAllRents += res.rent;		
		}
		
		// to get landlord rent as percentage of DB total and send data to piechart: 
		for (Landlord landlord : landlords) {
			double RentLandlord = (TotalRent(landlord)); // refers to method below
			
			// Refers to String percentage() method below:
			String str = percentage(RentLandlord, sumAllRents);
			//chart accepts data with the label followed by the number:
			Arrayjs.add(Arrays.asList(landlord.firstName + " " + landlord.lastName, str));
		}
		renderJSON(Arrayjs);
	}
		
/*		long percentDatabasePortfolioTotal = (sumResidences*100)/landlord.rentrollTotal;
		Logger.info(("percentage of total database portofolio rental income is: " + percentTotal);
		return String.valueOf(percentTotal);*/
	
	/**
	 * Method to find the individual landlords total rentroll from their various residences
	 * @param landlord
	 * @return
	 */
	public static double TotalRent(Landlord landlord){
		double sumLandlordRents = 0;
		List<Residence> residencesAll = Residence.findAll();
		
		for (Residence res : residencesAll) {
		if (res.landlord.equals(landlord)){
			sumLandlordRents += res.rent;
		}
		}
		return sumLandlordRents;		
	}
	/**
	 * Creates String value of residence rent as percentage of db total rental portfolio
	 * ind decimal format
	 * @param rent the rent for each residence
	 * @param sumAllRents the total database portofilio rental income
	 * @return
	 */
	public static String percentage(double rent, double sumAllRents){
		return Double.toString(Math.round((rent*100) / sumAllRents));
	}
}
