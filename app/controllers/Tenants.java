package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

/**
 * An Tenants class to inherit data and behaviour from the Controller class
 * Note: static methods may be invoked from any controller, minimising code
 * duplication.
 * 
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */
public class Tenants extends Controller {

	/**
	 * Renders the Tenant's tenancy status to the Landlord Configuration page.
	 * Appropriate to render the vacantResidence to the index entire page view rather 
	 * than in changeTenancy method only, as this list effects the google map view also.
	 */
	public static void index() {
		Tenant tenant = Tenants.getCurrentTenant();

		// to create a list of vacant residences, based on the Residence model
		// as done at Report controller:
		List<Residence> residencesAll = Residence.findAll();
		List<Residence> vacantResidences = new ArrayList<Residence>(); 
	
		// Fetch all residences and filter out those with no tenant(vacant residences)by:
		// go through all the residences and add the vacant ones to another list 'vacantResdiences', 
		// and pass that vacantResdiences list to the view.
		for (Residence res : residencesAll) {
			if (res.tenant == null) { // vacant residences
				// add to new 'residences' list to pass to the view
				vacantResidences.add(res);
			}
		}
		// 'tenant' to render the tenant and associated residence (eircode) if any
		// (to populate the eircode/terminate tenancy fields)
		// 'residences' passes the tenant and vacant residences to the view for
		// 'change tenancy' pulldown
		render(tenant, vacantResidences);
	}

	/**
	 * Creates the tenant signup for rendering in the Tenants signup view
	 */
	public static void signup() {
		render();
	}

	/**
	 * Creates the tenant signup for rendering in the Tenantss login view
	 * Registers a tenant onto the MyRent system
	 * 
	 * @param landlord
	 *            Simulates the logged-in landlord
	 */
	public static void register(Tenant tenant) {
		tenant.save();
		login();
	}

	/**
	 * Creates the tenant login for rendering in the tenants login view
	 */
	public static void login() {
		render();
	}

	/**
	 * Creates the tenant logout by diverting the tenant to the Welcome landing
	 * page
	 */
	public static void logout() {
		// session.clear(); STORY 10
		setSessionLogout();
		Logger.info("Tenant signed out");
		Welcome.index();
	}

	/**
	 * Affirms the current tenant by searching the firstName of tenant for the
	 * current session. Authenticates that a tenant's login credentials are
	 * valid
	 * 
	 * @param email
	 *            The submitted email address
	 * @param password
	 *            The corresponding password
	 * @return the currently logged-in landlord
	 **/
	public static void authenticate(String email, String password) {
		Logger.info("Attempting to authenticate with " + email + ":" + password);

		Tenant tenant = Tenant.findByEmail(email);
		if ((tenant != null) && (tenant.checkPassword(password) == true))
		// refers to Tenant model
		{
			Logger.info("Tenant authentication successful");
			session.put("logged_in_tenantid", tenant.id);
			// to ensure landlord cannot access value for tenantid and go to the
			// Tenant page:
			session.put("logged_in_landlordid", null);
			session.put("logged_status", "logged_in");
			Tenants.index();
		} else {
			Logger.info("Tenant authentication failed");
			login();
		}
	}

	/**
	 * Affirms the currently logged-in tenant
	 * @return returns the logged-in tenant
	 */
	public static Tenant getCurrentTenant() {
		String tenantId = session.get("logged_in_tenantid");
		if (tenantId == null) {
			return null;
		}
		Tenant logged_in_tenant = Tenant.findById(Long.parseLong(tenantId));
		// refers to tenant model:
		Logger.info("Logged in tenant is " + logged_in_tenant.firstName);
		return logged_in_tenant;
	}

	/**
	 * Clears session on logout
	 */
	protected static void setSessionLogout() {
		if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in")) {
			// session.clear(); STORY 10
			String TenantId = session.get("logged_in_tenantid");
			Tenant logged_in_tenant = Tenant.findById(Long.parseLong(TenantId));
			session.remove();
			Logger.info("Tenant " + logged_in_tenant.firstName + " is now logged out");
			session.put("logged_status", "logged_out");
		}
	}

	/**
	 * Terminates the Tenant tenancy or removes the residence object associated with the tenant object
	 */
	public static void terminateTenancy() {
		Tenant tenant = Tenants.getCurrentTenant();
		// Residence residence = Residence.findByEircode(eircode);
		Logger.info("Terminating tenancy of Tenant: " + tenant);
		// tenant.residence.remove();// to remove residence from tenant status -
		// doesn't work..??
		tenant.residence = null; // set residence to null to remove the
									// residence from tenant listing

		tenant.save();// refers to the database: saves tenant updated status,
						// with residence removed
		Tenants.index(); // refreshes the page to render the updated logged_in
							// tenant status
	}

	/**
	 * STORY 11. Changes tenant status to attach to vacant residence from vacantResdiences list
	 * @param eircode
	 */
	public static void changeTenancy(String eircode) {
		Tenant tenant = Tenants.getCurrentTenant();
		// List<Residence> vacantResidences = new ArrayList<Residence>();
		Logger.info("Changing tenancy of Tenant: " + tenant);
		
		// to search for a vacant residence in list created at Tenants.index above:
		Residence vacantResidence = Residence.findByEircode(eircode);
		// the new residence object now an object from the vacantResidence list:
		tenant.residence = vacantResidence;
		Logger.info("Tenant: " + tenant + " has new tenancy, at residence eircode: " + eircode);

		tenant.save();// refers to the database
		Tenants.index(); // render the page anew
	}

	
    /**
     * Find a list of vacant residences.
     * The list of (tenant = null) vacant residences assembled in equivalent of json array
     * comprise: residence eircode (geoObj[0]) , latitude (geoObj[1]), longitude   (geoObj[2]) 
     * and rented status message (geoObj[3]).
     * Renders all VacantResidences as a json array
     */
    public static void vacantresidences() {
    //List of vacantresidences returned as part of <Residence> residences >() list
    	
	List<Residence> residencesAll = Residence.findAll();
      // reportMapCircle javascript requires an array of strings:
      List<ArrayList<String>> jsonArray = new ArrayList<ArrayList<String>>();

      for (Residence res : residencesAll) {
			if (res.tenant == null) {   
        ArrayList<String> s = new ArrayList<String>();

        s.add(res.eircode);
     // latitude and longitude defined within geolocation; getGeolocation a method in Residence model:
        s.add(Double.toString(res.getGeolocation().getLatitude())); 
        s.add(Double.toString(res.getGeolocation().getLongitude())); 
        s.add("Vacant Residence");   // to add a status messagee     
        jsonArray.add(s);
        Logger.info("Vacant Residences " + res.eircode);   
      }
      }
      renderJSON(jsonArray);
    }  
}
