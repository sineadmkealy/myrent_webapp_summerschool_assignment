package controllers;

import java.util.Date;
import java.util.List;

import controllers.Landlords;
import models.Residence;
import models.Tenant;
import models.Landlord;
import play.*;
import play.mvc.Controller;

import org.json.simple.JSONObject;

/**
 * The Residence class to inherit data and behaviour from the
 * Controller class
 * 
 * @author Sinead
 * @version 2.0 June 2016
 */
import play.db.jpa.Model;

public class InputData extends Controller {

	/**
	 * Render data input view (logged-in users only). Affirms the logged-in
	 * landlord by reference to the Landlords class. Renders this landlord and
	 * their residence in the InputData view
	 */
	/*
	 * public static void index() { Landlord landlord =
	 * Landlords.getCurrentlandlord(); if (landlord == null) { Logger.info(
	 * "Residence class : Unable to getCurrentlandlord"); Landlords.login(); }
	 * else { Logger.info("Residence :  Currentlandlord identified");
	 * render(landlord); } }
	 */

	/**
	 * SOLUTION: Render data input view (logged-in users only).
	 */
	public static void index() {
		if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in")) {
			Logger.info("Residence :  Currentlandlord identified");
			render();
		} else {
			Logger.info("Residence class : Unable to getCurrentlandlord");
			Landlords.login();
		}
	}

	/**
	 * SOLUTION [no addData method) 
	 * Updates the Residence model with residential
	 * data. The Play templating language creates a Residence object using the
	 * various fields in InputData.index (which captures the fields in Residence
	 * model). It passes this object to the controller as an argument in
	 * InputData.capture
	 */
	/*
	 * public static void capture(Residence residence){ Landlord landlord =
	 * Landlords.getCurrentLandlord();// method in Landlords controller
	 * residence.addLandlord(landlord); residence.dateRegistered = new Date();
	 * residence.save();
	 * 
	 * Logger.info("Residence data received and saved"); Logger.info(
	 * "Residence type: " + residence.residenceType); Logger.info("Rented? " +
	 * residence.rented);
	 * 
	 * index(); // to render the landlord status after residence added } 
	 * // AJAX : to post a notification message on successful return from the Ajax call
	 * CALL - REPLACE Index() above 
	 * //JSONObject obj = new JSONObject(); 
	 * //String value = "Congratulations. You have successfully registered your " + residence.residenceType +"."; 
	 * // obj.put("inputdata", value); //
	 * renderJSON(obj); // }
	 */
	/**
	 * Method to determine if landlord an existing registered landlord and
	 * direct to register or proceed to add data to landlord and residence
	 * profiles
	 * 
	 * @param rented
	 *            If property rented or vacant
	 * @param rent
	 *            The rent amount in euro
	 * @param numberBedrooms
	 *            The number of bedrooms in the residence
	 * @param residenceType
	 *            Residence may be apartment, studio or house types
	 * @param geolocation
	 *            Geolocation properties in format (latitude, longditude)
	 */
	public static void capture(long rent, long numberBedrooms, int numberBathrooms, int area, String residenceType,
			String geolocation, String eircode) { //, Tenant tenant
		// landlord object captured automatically so not listed
	// STORY 10: Need to know if tenant exists to determine if rented or not...???
		Logger.info("rent amount: " + rent + " , number of bedrooms " + numberBedrooms + " , number of bathrooms"
				+ numberBathrooms + " , floor area(sq.m.)" + area + " , residence type " + residenceType
				+ " , geolocation" + geolocation);

		Landlord landlord = Landlords.getCurrentLandlord();// method inLandlords controller
		if (landlord == null) {
			Logger.info("Residence class : Unable to getCurrentLandlord");
			Landlords.login();
		} else {
			addData(rent, numberBedrooms, numberBathrooms, area, residenceType, geolocation, eircode, landlord);// Added in story 10// tenant
		}
		// index(); // to render the landlord status after residence added
		Landlords.index();
	}

	/**
	 * Method to add landlord data to landlord profile, residence list and database
	 * @param rented
	 *            If property rented or vacant
	 * @param rent
	 *            The rent amount in euro
	 * @param numberBedrooms
	 *            The number of bedrooms in the residence
	 * @param residenceType
	 *            Residence may be apartment, studio or house types
	 * @param geolocation
	 *            Geolocation properties in format (latitude, longditude)
	 */
	private static void addData(long rent, long numberBedrooms, int numberBathrooms, int area, String residenceType,
			String geolocation, String eircode, Landlord landlord) { //, Tenant tenant
		Residence list = new Residence(rent, numberBedrooms, numberBathrooms, area, residenceType, geolocation, eircode, landlord, null);  //same as residence fields  
			
		// Works when tenant set to null, but not accepting list otherwise despite new dialogue box in InputData.index???????????????
		list.save();
		Logger.info("Data for residence saved");
	}
}


