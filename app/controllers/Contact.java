package controllers;

import play.Logger;
import play.mvc.Controller;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import models.Landlord;

import models.*;

/**
 * An Landlords class to inherit data and behaviour from the Controller class.
 * This class configured only for localhost.
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */
public class Contact extends Controller {
	/**
	 * Establishes the contact page for render to the view.
	 * This controller receives the contact message feedback and issue an acknowledgememt.
	 */
	public static void index() {
		render();
	}

	/**
	 * Sends acknowledgement to landlord that their message received. Assumes that a
	 * message can also be sent by a site visitor, who is not already a
	 * signed-up landlord.
	 * 
	 * @param landlord
	 *            : an existing signed up landlord or an enquirer saved as new landlord
	 *            to enable message to be sent
	 */
/*		public static void saveMessage(String firstName, String lastName, String email, String messageTxt){		
			Message list = new Message(firstName, lastName, email, messageTxt); 
			list.save();
			Logger.info("Saving landlord message: " + messageTxt);
			render("Contact/sendAcknowledgement.html"); //  // to render the landlord status after after message saved
}*/
	//Associated route removed: POST   /contact/sendAcknowledgement				Contact.saveMessage

	  /**
	   * Permits testing with external email address using Javax mail
	   * @param firstName
	   *          : the message sender's first and
	   * @param lastName
	   *          : last name.
	   * @param emailSender
	   *          : the email address of the sender of the message.
	   * @param messageTxd
	   *          : the message.
	   */
	 
	  public static void sendMessage(Landlord landlord, String messageTxt){

	    if (Landlords.getCurrentLandlord() == null) {
	      Landlords.login();
	    }
	    final String landlordname = "sineadmkealy@yahoo.ie";
	    final String password = "password";

	    Properties props = new Properties();
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.starttls.enable", "true");
	    props.put("mail.smtp.host", "smtp.mail.yahoo.com");
	    props.put("mail.smtp.port", "587");

	    Session session = Session.getInstance(props, new javax.mail.Authenticator() {
	      protected PasswordAuthentication getPasswordAuthentication() {
	        return new PasswordAuthentication(landlordname, password);
	      }
	    });

	    try {
	      String forwarderAddress = landlordname;
	      String destinationAddress1 = landlord.email;
	      String destinationAddress2 = landlordname;
	      Message message = new MimeMessage(session);
	      message.setFrom(new InternetAddress(forwarderAddress));
	      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinationAddress1));
	      message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(destinationAddress2));
	      message.setSubject("Message for MyRent Webmaster");
	      message.setText(messageTxt);

	      Transport.send(message);

	    }
	    catch (MessagingException e)    {
	      Logger.info(e.getMessage());
	      Acknowledgement.index();// a temporary fix: an exception caught when
	                              // sendMessage invoked from Cloud
	      throw new RuntimeException(e);
	    }

	    Acknowledgement.index();
	  }
	}
	
	

