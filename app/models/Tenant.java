package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import play.db.jpa.Blob;
import play.db.jpa.Model;

/**
 * The tenant class to inherit data and behaviour from the Model class
 * 
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */


/* //One tenant can only rent one residence
 @OneToOne(mappedBy = "residence" , cascade = CascadeType.ALL) 
List<Residence> residences = new ArrayList<Residence>(); ;*/

/*@OneToOne(cascade = CascadeType.ALL);
List<Residence> residences = new ArrayList<Residence>();*/


@Entity // indicates information to be stored to a database
// this landlord class represented by a table in the database
public class Tenant extends Model {
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	//public String residence;// COMMENTED OUT AT STORY 8 AS NEW Residence residence field introduced
	
	//@OneToOne(mappedBy = "tenant", cascade=CascadeType.ALL) //Story 8 ; mapping added story_11// cascade removed
	//deleting the tenant won't delete any associated residences:
	//@OneToOne(mappedBy = "tenant")
	//@OneToOne(mappedBy = "tenant", cascade = CascadeType.DETACH)
	//@PrimaryKeyJoinColumn
	@OneToOne
	public Residence residence; // One tenant per residence / one residence per tenant 

	// STORY 10: VACANT RESIDENCE HAS NO TENANT
	/**
	 * Establishes the business logic and data required by the landlord model
	 * 
	 * @param firstName
	 *            the landlord first name
	 * @param lastName
	 *            the landlord last name
	 * @param email
	 *            the landlord's email
	 * @param password
	 *            the landlord's login password
	 */
	public Tenant(String firstName, String lastName, String email, String password, Residence residence) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	    this.residence = residence;
	}

	/**
	 * Method to identify the tenant by reference to their email address
	 * 
	 * @param email
	 *            the email address actual parameter passed in by the landlord to
	 *            search by
	 * @return returns the landlord email in the first instance
	 */
	public static Tenant findByEmail(String email) {
		return find("email", email).first();
	}

	/**
	 * Method to check that the actual parameter email address submitted matches
	 * that of the tenant email passed in at signup
	 * 
	 * @param password
	 * @return returns true if match exits or false if no match
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	/**
	 * Method to override the toString method for Report list.
	 */
	public String toString() {
		return firstName + " " + lastName;
	}
}