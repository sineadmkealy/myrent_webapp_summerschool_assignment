package models;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.Model;

public class Filters extends Model {
	public String filter;
	public List<String> byRented;
	public List<String> byRent;
	public List<String> byType;
	public List<String> byDateRegistered; // byAll: not use in end; AdminReports.index() ok
	
	public Filters() {
		byRented = new ArrayList<String>();
		byRented.add("rented");
		byRented.add("vacant");
		
		byRent = new ArrayList<String>();
		byRent.add("Sort_ascending");
		byRent.add("Sort_descending");
		
		byType = new ArrayList<String>();
		byType.add("Apartment|Flat");
		byType.add("Studio");
		byType.add("House");
		
		// Not used in end:
		byDateRegistered = new ArrayList<String>();
		byDateRegistered.add("normal order");
		byDateRegistered.add("reverse order");
		
	}
}
