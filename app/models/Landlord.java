package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.jpa.Blob;
import play.db.jpa.Model;

/**
 * The landlord class to inherit data and behaviour from the Model class
 * 
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */
//PARENT ENTITY
@Entity // indicates information to be stored to a database
// this landlord class represented by a table in the database
public class Landlord extends Model {
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public String address1;
	public String address2;
	public String city;
	public String county;
	public String postcode;
	
	//Sotyr_16:
	public long rentrollTotal;

	// STORY 6: One landlord can own many residences : [TENANT PERSPECTIVE]
	// new field residences (a list of properties own by Landlord) to correspond 
	// with arraylist in landlords.index controller
	// maps to 'Landlord from' field already declared in Residence model
	// Used to populated dropdown eircode menus for add /delete residence in
	// Landlords.index view
	// COMMENTED OUT AT STORY 8 AS SIMLAR ARRAYLIST INTRODUCED:
	//======================================================================
	//@OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
	// public List<Residence> residences = new ArrayList<Residence>();
	//=========================================================================
	 //alt. more concise...: @OneToMany(cascade = CascadeType.ALL) public List<Residence> residences;

	// STORY 8: One landlord can own many residences [LANDLORD PERSPECTIVE]
	// maps to field declared in Residence model
	// Landlord (and Tenant?) models are the PARENT; Residence model the CHILD
	//https://docs.oracle.com/cd/E19798-01/821-1841/bnbqm/index.html
	//All cascade operations will be applied to the parent (Landord) entity�s related entity (Residence). 
	// All is equivalent to specifying cascade={DETACH, MERGE, PERSIST, REFRESH, REMOVE}
	// The landlord MODEL has the cascade, so it will delete any associated residences when it is deleted
	@OneToMany(mappedBy = "landlord", cascade = CascadeType.ALL)
	public List<Residence> residences = new ArrayList<Residence>();

	// Story 16: one landlord receives may rents on their various residences NOT: use Residence above list
/*	@OneToMany(mappedBy = "landlord", cascade = CascadeType.ALL)
	public List<Rentroll> rentrolls = new ArrayList<Rentroll>();*/
	/**
	 * Establishes the business logic and data required by the landlord model
	 * 
	 * @param firstName
	 *            the landlord first name
	 * @param lastName
	 *            the landlord last name
	 * @param email
	 *            the landlord's email
	 * @param password
	 *            the landlord's login password
	 */
	public Landlord(String firstName, String lastName, String email, String password, String address1, String address2,
			String city, String county, String postcode) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.county = county;
		this.postcode = postcode;
	}

	/**
	 * Method to identify the landlord by reference to their email address
	 * 
	 * @param email
	 *            the email address actual parameter passed in by the landlord
	 *            to search by
	 * @return returns the landlord email in the first instance
	 */
	public static Landlord findByEmail(String email) {
		return find("email", email).first();
	}

	/**
	 * Method to check that the actual parameter email address submitted matches
	 * that of the landlord email passed in at signup
	 * 
	 * @param password
	 * @return returns true if match exits or false if no match
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	/**
	 * Method to override the toString method for Report list.
	 * <td>${landlord}</td> would output landlord[1] otherwise. Note: not needed
	 * in our edition.
	 */
	public String toString() {
		return firstName + " " + lastName;
	}
	 /**
	  * Add each resdience.rent to the landlord's total rentroll
	  * @param rentroll
	  */
	public void addRent(Residence residence){
		this.residences.add(residence);
	}
/*		public void addRentroll (Rentroll rentroll){
			this.rentrolls.add(rentroll);*/
		
		//residence.rent
		
		
	}
	
