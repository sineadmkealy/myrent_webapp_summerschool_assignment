package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;
import utils.LatLng;

/**
 * The Residence class to inherit data and behaviour from the Controller class
 * 
 * @author Sinead
 * @version 2.0 June 2016
 */
//The Landlord and Tenant models are the parents, and the Residence model is the child.
// @JoinColumn, which is used on the child entity (Residence model)

@Entity // indicates information to be stored to a database
// this landlord class represented by a table in the database
public class Residence extends Model {
	//@ManyToOne
	//public Landlord from; obsolete in Story 8 // many residences can be 'owned' by one landlord [TENANT PERSPECTIVE]
	//public Boolean rented; REMOVE STORY 9
	public long rent;
	public long numberBedrooms;
	public int numberBathrooms;
	public int area; // area in sq.m.
	public String residenceType;
	public String geolocation;
	public Date dateRegistered; // date stamp - auto-generated
	public String eircode;

	// residence model uses the @JoinColumn to map back to the landlord and tenant models.
	// @JoinColumn, which is used on the child entity
	//@JoinColumn(name = "landlord_id")
	@ManyToOne // Story 8
	public Landlord landlord; // Landlord may own many residences [LANDLORD PERSPECTIVE]
	
	//@OneToOne(mappedBy = "residence", cascade=CascadeType.ALL)  // Story 8
	//CascadeType.ALL means the persistence will propagate (cascade) all EntityManager operations 
	//(PERSIST, REMOVE, REFRESH, MERGE, DETACH) to the relating entities (tenant). If residence deleted we 
	// do not want the tenant to also be deleted so STORY 11 :
	// remove the cascade attribute to allow (res.tenant == null) to find vacant properties.
	//Story_13: Removed the mappedBy on the Tenant, because this is now on the Residence property of the Tenant model.
	//@OneToOne(mappedBy = "residence")	
	//@OneToOne
	//@JoinColumn(name = "tenant_id")
	// Removes both side of relationship form db when Residence is deleted - ie removes residence and tenant also - CORRECT???
	//@OneToOne(mappedBy = "residence", cascade=CascadeType.ALL) 
	//@OneToOne(mappedBy = "residence")	
	//http://uaihebert.com/jpa-onetoone-unidirectional-and-bidirectional/
	// The residence  MODEL has the cascade(mappedBy = "residence"), so it will delete any associated tenants when it is deleted
	@OneToOne(mappedBy = "residence", cascade=CascadeType.ALL) // mapped by means residence the owner ofthe  relationship, the stronger side
	public Tenant tenant; // One tenant per residence / one residence per tenant 

	/**
	 * Establishes the business logic and data fields required by the residence
	 * model. Creates the individual residence objects /instance of residence.
	 * 
	 * @param from
	 *            The landlord/owner/agent of the residence
	 * @param rented
	 *            If property rented or vacant
	 * @param rent
	 *            The rent amount in euro
	 * @param numberBedrooms
	 *            The number of bedrooms in the residence
	 * @param residenceType
	 *            Residence may be apartment, studio or house types
	 * @param geolocation
	 *            Geolocation properties in format (latitude, longditude)
	 */
	public Residence(long rent, long numberBedrooms, int numberBathrooms, int area,
			String residenceType, String geolocation, String eircode, Landlord landlord, Tenant tenant) {
		this.rent = rent;
		this.numberBedrooms = numberBedrooms;
		this.numberBathrooms = numberBathrooms;
		this.area = area;
		this.residenceType = residenceType;
		this.geolocation = geolocation;
		dateRegistered = new Date();
		this.eircode = eircode;
		this.landlord = landlord;
		this.tenant = tenant;
	}

	/**
	 * 
	 * @return Returns the geolocation of the current residence in the form of a
	 *         LatLng object. Returns the string latitude and longditude
	 *         coordinates for the geolocation of the residence object. The
	 *         geolocation field in Residence, obtained from the view input,
	 *         comprises a concatenation of 3 strings - 'latitude' + '', +
	 *         'longitude'. The method LatLng.toLatLng splits this into a pair
	 *         of double values.
	 */
	public LatLng getGeolocation() {
		// return null;
		// the LatLing object, converted to string LatLng (calls the LatLng
		// toLatLng(String latlng)
		// method in LatLng.jav utils), to convert the coordinates back to
		// double values.
		return LatLng.toLatLng(this.geolocation);
	}

	/**
	 * SOLUTION: neccessary for InputData.capture to pass in the residence object. Allows
	 * update of models with residential data
	 * 
	 * @param user
	 */
	public void addLandlord(Landlord landlord) {
		this.landlord = landlord;
		this.save();
	}

	/*
	 * Identifies residence by eircode
	 */
	public static Residence findByEircode(String eircode) { // as per Landlord findByEmail()
		return find("eircode", eircode).first();
	}

/*	public void remove(
			) {
		// TODO Auto-generated method stub
		
	}*/
}