package models;

import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity // indicates information to be stored to a database
public class Administrator extends Model{
	public String email;
	public String password;

	// STORY 111: VACANT RESIDENCE HAS NO TENANT
	/**
	 * Establishes the business logic and data required by the landlord model
	 * 
	 * @param email
	 *            the administrator's email
	 * @param password
	 *            the administrator's login password
	 */
	public Administrator(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public static Administrator findByEmail(String email) {
		return find("email", email).first();
	}
	
	/**
	 * Method to check that the actual parameter email address submitted matches
	 * that of the Administrator email passed in at login
	 * 
	 * @param password
	 * @return returns true if match exits or false if no match
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}	
}
